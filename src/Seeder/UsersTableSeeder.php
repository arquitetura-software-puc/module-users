<?php

namespace Mgzaspuc\Users\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id_people' => 1,
            'name' => 'ADMIN',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin123'),
            'group' => 'admin',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'id_people' => 2,
            'name' => 'Seller',
            'email' => 'seller@seller.com',
            'password' => Hash::make('seller123'),
            'group' => 'seller',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'id_people' => 3,
            'name' => 'Client',
            'email' => 'client@client.com',
            'password' => Hash::make('client123'),
            'group' => 'client',
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
