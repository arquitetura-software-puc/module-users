<?php

namespace Mgzaspuc\Users\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_people' => 'required',
            'name' => 'required|unique:users|max:191',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|max:191',
            'group' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id_people.required' => 'O campo Pessoa não pode ser vazio',
            'name.required' => 'O campo Nome do Usuário não pode ser vazio',
            'name.unique' => 'O Nome do Usuário já existe cadastrado no sistema',
            'name.max' => 'O Nome do Usuário não pode ter mais de 191 caracteres',
            'email.required' => 'O campo Email não pode ser vazio',
            'email.email' => 'O Email informado é inválido',
            'email.unique' => 'O Email já existe cadastrado no sistema',
            'email.max' => 'O Email não pode ter mais de 191 caracteres',            
            'password.required' => 'O campo Senha não pode ser vazio',
            'password.max' => 'A Senha não pode ter mais de 191 caracteres',            
            'group.required' => 'O campo Grupo não pode ser vazio',
        ];
    }
}
