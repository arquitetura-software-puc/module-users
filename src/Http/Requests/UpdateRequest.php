<?php

namespace Mgzaspuc\Users\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_people' => 'required',
            'name' => 'required|max:191',
            'email' => 'required|email|max:191',
            'password' => 'max:191',
            'group' => 'required'
        ];
    }

    public function messages() {
        return [
            'id_people.required' => 'O campo Pessoa não pode ser vazio',
            'name.required' => 'O campo Nome do Usuário não pode ser vazio',
            'name.max' => 'O Nome do Usuário não pode ter mais de 191 caracteres',
            'email.email' => 'O Email informado é inválido',
            'email.max' => 'O Email não pode ter mais de 191 caracteres',            
            'password.max' => 'A Senha não pode ter mais de 191 caracteres',            
            'group.required' => 'O campo Grupo não pode ser vazio',
        ];   
    }
}
