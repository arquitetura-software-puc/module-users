<?php

namespace Mgzaspuc\Users\Http\Controllers;

use Mgzaspuc\Users\Http\Requests\StoreRequest;
use Mgzaspuc\Users\Http\Requests\UpdateRequest;
use Illuminate\Http\Request;
use Mgzaspuc\Users\User;
use Mgzaspuc\People\People;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = new User();
        $listUsers = $users->paginate(15);
        
        return view('users.index', compact('listUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $listUsers = $user->all();
        $listGroups = $this->getListGroup();
        $people = new People();
        $listPerson = $people->all();
        
        return view('users.create', compact('listUsers', 'listGroups', 'listPerson'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try{        
            $user = new User();
            $user->id_people = $request->get('id_people');
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->password = Hash::make($request->get('password'));
            $user->group = $request->get('group');
            $user->created_at = new \DateTime();
            $user->updated_at = new \DateTime();
            $user->save();
        
            return redirect('/usuarios')
                ->with('success', 'Registro criado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/usuario/novo')
                ->with('error', 'Não foi possível criar o registro!' . $ex->getMessage())
                ->withInput($request->input());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = new User();
        $user = $users->find($id);
        $listGroups = $this->getListGroup();
        $people = new People();
        $listPerson = $people->all();        
        
        return view('users.edit', compact('user', 'listGroups', 'listPerson'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request)
    {
        try{
            $users = new User();
            $user = $users->find($request->input('id'));            
            $user->id_people = $request->get('id_people');
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->password = $request->get('password') 
                ? Hash::make($request->get('password')) 
                : $user->password;
            $user->group = $request->get('group');
            $user->updated_at = new \DateTime();
            $user->save();
        
            return redirect('/usuarios')
                ->with('success', 'Registro alterado com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/usuario/editar/' . $request->input('id'))
                ->with('error', 'Não foi possível alterar o registro!');
        }
    }      
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $users = new User();
            $user = $users->find($request->input('id'));        
            $user->delete();       
            
            return redirect('/usuarios')
                ->with('success', 'Registro excluído com sucesso!');            
            
        } catch (\Exception $ex) {
            return redirect('/usuarios')
                ->with('error', 'Não foi possível excluir o registro!');
        }
    }
    
    private function getListGroup()
    {
        return ['admin', 'client', 'seller'];
    }
}
