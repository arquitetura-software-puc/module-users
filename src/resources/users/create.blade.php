@extends('layouts.admin')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Usuários</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Usuários</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-3">
                <!-- MENUS AND FILTERS-->
                <div class="panel panel-default sidebar-menu">
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Menu</h3>
                    </div>
                    <div class="panel-body">
                        @include('elements.admin_menu')
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <h2>Cadastrar Usuário</h2>
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="post" action="{{ route('usuario_salvar') }}">
                    @csrf
                    
                    <div class="form-group">    
                        <label for="id_people">Pessoa:</label>
                        <select id="id_people" name="id_people" class="form-control">
                            <option name="">Selecione</option>  
                            @foreach($listPerson as $people)
                                @if($people->id == old('id_people'))
                                <option selected value="{{$people->id}}">{{$people->name}}</option>  
                                @else
                                <option value="{{$people->id}}">{{$people->name}}</option>  
                                @endif
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group">    
                        <label for="name">Nome do Usuário:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}"/>
                    </div>
                    
                    <div class="form-group">    
                        <label for="group">Grupo:</label>
                        <select id="group" name="group" class="form-control">
                            <option name="">Selecione</option>  
                            @foreach($listGroups as $group)
                                @if($group == old('group'))
                                <option selected value="{{$group}}">{{$group}}</option>  
                                @else
                                <option value="{{$group}}">{{$group}}</option>  
                                @endif
                            @endforeach
                        </select>
                    </div>                       

                    <div class="form-group">    
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}"/>
                    </div>

                    <div class="form-group">    
                        <label for="password">Senha:</label>
                        <input type="password" class="form-control" id="password" name="password" value=""/>
                    </div>
                    
                    <button type="submit" class="btn btn-default">Cadastrar</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection
